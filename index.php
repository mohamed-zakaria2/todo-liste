<?php
// index.php

// Database connection parameters.
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'todolist');
define('DB_HOST', '127.0.0.1');
define('DB_PORT', '3307');

// Establish a database connection using PDO.
try {
    $pdo = new PDO("mysql:host=" . DB_HOST . ";port=" . DB_PORT . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Database connection failed: " . $e->getMessage());
}
// Initialize the message variable.
$message = '';
// Handle actions (new, delete, done, undo, clear).
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $action = isset($_POST['action']) ? $_POST['action'] : null;

    switch ($action) {
        case 'new':
               $title = isset($_POST['title']) ? trim ($_POST['title']) : ''; // Get title or set an empty string if not provided
               if (!empty($title)) { // Check if the title is not empty
                $stmt = $pdo->prepare("INSERT INTO todo (title) VALUES (:title)");
                $stmt->bindParam(':title', $title);
                $stmt->execute();

                // Fetch the latest task after adding.
                $stmt = $pdo->prepare("SELECT * FROM todo ORDER BY created_at DESC LIMIT 1");
                $stmt->execute();
                $latestTask = $stmt->fetch(PDO::FETCH_ASSOC);
               } else {
                $message = 'Task cannot be empty. Please enter a task.';
            }
            
            break;
        case 'delete':
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
                $stmt = $pdo->prepare("DELETE FROM todo WHERE id = :id");
                $stmt->bindParam(':id', $id);
                $stmt->execute();
            }
            break;
            case 'done':
                if (isset($_POST['id'])) {
                    $id = $_POST['id'];
                    $stmt = $pdo->prepare("UPDATE todo SET done = 1 WHERE id = :id");
                    $stmt->bindParam(':id', $id);
                    $stmt->execute();
                }
                break;
                case 'undo':
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];
                        $stmt = $pdo->prepare("UPDATE todo SET done = 0 WHERE id = :id");
                        $stmt->bindParam(':id', $id);
                        $stmt->execute();
                    }
                    break;
                    //add button
                    case 'clear':
                        // This action is handled by JavaScript
                        break;
                        case 'clearconfirmed':
                            $stmt = $pdo->prepare("TRUNCATE TABLE todo");
                            $stmt->execute();
                            break;
        
              
            
    }
}

// Read tasks from the 'todo' table.
$stmt = $pdo->prepare("SELECT * FROM todo ORDER BY created_at DESC");
$stmt->execute();
$taches = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo List</title>
    <!-- Bootstrap CDN -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<nav class="navbar navbar-expand-lg ">
    <a class="navbar-brand" href="#">Todo List</a>
</nav>

<div class="container mt-4">
    <!-- Display the message -->
    <?php if (!empty($message)) : ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?= $message ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>

    <form class="form" action="index.php" method="post">
        <div class="form-group">
            <label class="label" for="title">New Task:</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>
        <div class="buttons">
        <button type="submit" class="btn-add" name="action" value="new">Add Task</button>
        <button type="button" class="btn-clear"  id="clearBtn">Clear</button>
        </div>
    </form>

    <!-- Display the latest added task immediately -->
    <?php if (isset($latestTask)) : ?>
        <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
            Task added: <?= $latestTask['title'] ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?> 

    <ul class="list-group mt-4">
        <?php foreach ($taches as $tache) : ?>
            <li class="list-group-item <?= $tache['done'] ? 'list-group-item-success' : 'list-group-item-warning' ?>">
                <?= $tache['title'] ?>
                <form action="index.php" method="post" class="float-right">
                    <input type="hidden" name="id" value="<?= $tache['id'] ?>">
                    <?php if ($tache['done']) : ?>
                        <button type="submit" class="btn-undo" name="action" value="undo">Undo</button>
                    <?php else : ?>
                        <button type="submit" class="btn-done" name="action" value="done">Done</button>
                    <?php endif; ?>
                    <button type="submit" class="btn-delete" name="action" value="delete">Delete</button>
                </form>
            </li>
        <?php endforeach; ?>
    </ul>

      <!-- Hidden form for clear action -->
      <form id="clearForm" action="index.php" method="post" style="display: none;">
        <input type="hidden" name="action" value="clearconfirmed">
    </form>

    
    <!-- Hidden form for clear action -->
    <form id="clearForm" action="index.php" method="post" style="display: none;">
        <input type="hidden" name="action" value="clearconfirmed">
    </form>

    <!-- Clear Confirmation Alert Modal -->
    <div class="modal fade" id="clearConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="clearConfirmationModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="clearConfirmationModalLabel">Clear All Tasks</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to clear all tasks? This action cannot be undone.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-add" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn-clear" id="clearConfirmBtn">Clear</button>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Bootstrap JS and Popper.js CDN -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<!-- JavaScript for confirmation alert -->
<script>
    document.getElementById('clearBtn').addEventListener('click', function() {

       
        $('#clearConfirmationModal').modal('show');
    });

    document.getElementById('clearConfirmBtn').addEventListener('click', function() {
        document.getElementById('clearForm').submit(); // Trigger the hidden form submission

    });
</script>
</body>
</html>

